/* 
Wait for DOM content
*/
document.addEventListener('DOMContentLoaded', () => {
    /* 
    # Mise en place d'un lecteur de support de cours
    L'exercice qui vous est proposé va vous permettre de réaliser une WebApp qui affiche les supports de cours qui vous ont été fournis lors de vos sessions. Les principes à mettre en place sont exactement les mêmes que ceux vue en cours, aucun piège dans cette exercice, il vous suffit d'être capable de répéter ce que vous avez vu en cours dans un cadre légèrement différent. Et en vous proposant de découvrir plus en profondeur le format Markdown. 
    
    Ce format est celui utilisé pour vos supports de cours, mais aussi pour les fichiers README que vous devez créer pour chacun de vos répetoires. Le principe de ce format est qu'il est facile a convertir dans d'autres formats, par exemple en HTML. Ce que vous allez devoir faire dans cet exercice et d'éxécuter une requête vers des fichier Markdown, pour les afficher en HTML dans votre page. Pour ce faire, vous avez pour commencer toutes les techniques vues en cours, puis le module Marked (https://www.npmjs.com/package/marked), qui est déja chargé dans le fichier 'index.html'. Afficher la page web dans votre navigateur et inspecter votre console, l'intéret du format Markdonw est la simplicité avec laquelle on peut le convertir.

    Prennez le temps de bien lire l'ennoné suivant, et bon courage!

    ## Liste des tâches :
    - Capter l'événement clic sur chaque bouton de la section 'section-courses-list'
    - Une fois l'événement capté, récupérer la valeur de la propriété 'data-link'
    - Utiliser la valeur de 'data-link' pour exécuter une requête HTTP afin de récupérer le contenu d'un support de cours
    - Une fois le contenu récupéré, créer une section dans le DOM pour l'afficher avec Marked
    */

    // Exemple d'utilisation du module Marked
    console.log( marked.parse("# Hello **markdown**, I am an H1 *markup*") );
    
    const debug = false;

    let mainSections = {
        'section-courses-list' : document.querySelector('#section-courses-list')
    }


    const getHTTPRequest = (endpoint, section) => {
        
        const fetchRequest = new FetchRequest(
            `${endpoint}`, 
            'json'
        )

        fetchRequest.init()

        fetchRequest.sendRequest()
        .then( fetchResponse => {
            // Get section index
            let sectionArray = [];
            for( let prop in mainSections ){
                sectionArray.push( mainSections[prop] )
            }

            let domContent = createElements(endpoint, fetchResponse)

            // Add list item in the section and display the section
            section.appendChild( domContent[0] );

            // Debug
            if(debug){
                console.log('From OBJECT', endpoint, section)
            }

        })
        
    }
    const onClick = (type, id) => {
        // Check type to fetch the correct values
        if( type === 'data-link' ){

            getJsonCollection(`{data-link}`, mainSections['section-courses-list'])
        }
        button.addEventListener('click', event => {
            let button = document.getElementsByTagName('button')
            button.setAttribute('data-list', item.id)
            let objectType = undefined;
            if( endpoint.indexOf('item.id') !== -1 ){
                objectType = 'item.id'
            }
            // Bind click
            onClick(objectType, event.target.getAttribute('data-list'))

            if(onClick === true){
                mainSections['data-list'] = getHTTPRequest;
            }
        })
         // Debug
         if(debug){
            console.log('onClick', type, id)
        }
    }
    


})
